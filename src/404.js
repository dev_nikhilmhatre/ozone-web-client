import React from 'react'
import './404.css'
import { Link } from 'react-router-dom'
import worried from './media/worried.svg';
import tongue from './media/tongue.svg';

const Erro404 = () => (
    <div id="div404">
        <h1 id="h1404">404</h1>
        <p id="p404">
            Oops! Look like you have lost your way.<img src={worried} alt="worried" height="20" weight="20" />
        <br />
            Not to worry we'll help you to get home safely.<img src={tongue} alt="worried" height="20" weight="20" />
        </p>
        <Link className="button404" to='/shop'><i className="icon-home"></i>Home</Link>
    </div>
)
export default Erro404