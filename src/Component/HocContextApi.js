import React, { Component } from 'react';
import {Context} from './App'

// HOC for context api
const HocContextApi = (WrappedComponent) => {
    return class HocContextApi extends Component {
        render() {
            return (
                <Context.Consumer>
                    {(context) => <WrappedComponent context={context} {...this.props} />}
                </Context.Consumer>
            )
        }
    }
}

export default HocContextApi;