import React from 'react';
import logo from '../media/LOGO_OZONE.png';


const Logo = (props) => (
    <img src={logo} alt="logo" width="170" height="43" />
)

export default Logo
