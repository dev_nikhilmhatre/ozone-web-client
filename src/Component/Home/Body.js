import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid'
import SubBody from './SubBody'
import img4 from '../../media/img4.jpg';
import img5 from '../../media/img5.jpg';
import Paper from '@material-ui/core/Paper';

const textColor = {
  color: 'rgba(0, 0, 0, 0.54)',
  margin: 0,
  overflow: 'auto'
}

const paper = {
  margin: '0% 1%',
  padding: '10px',
  height: 'auto',
  textAlign: 'center'
}

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    minWidth: 300,
    width: '100%'
  },
  image: {
    position: 'relative',
    height: "70vh",
    [theme.breakpoints.down('xs')]: {
      width: '100% !important', // Overrides inline-style
      height: 280,
    },
    '&:hover, &$focusVisible': {
      zIndex: 1,
      '& $imageBackdrop': {
        opacity: 0.7,
      },
      '& $imageMarked': {
        opacity: 0,
      },
      '& $imageTitle': {
        border: '4px solid currentColor',
      },
    },
  },
  focusVisible: {},
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
    textDecoration: 'none',
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    color: 'white',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 4}px ${theme.spacing.unit + 6}px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
});

const images = [
  {
    url: img4,
    title: 'Ozone Health Freac',
    width: '96%',
    link: '/shop/',
    address: ` Shop No. 6, New Kadambari Society, Naka Signal, Charai,
    Thane West, Panch Pakhdi, Thane West, Thane, Maharashtra 400602.`
  },
  {
    url: img5,
    title: 'Ozone Fitness Center',
    width: '96%',
    link: '/gym/',
    address: `Achalkuti Society, Mahatma Gandhi Rd, Hindu Colony, Shivaji Nagar,
    Thane West, Maharashtra 400602.`
  },
];

// Two button of images
function Body(props) {
  const { classes } = props;
  return (
    // <Grow in={true} timeout={1250}>
    <Grid container className={classes.root} style={{ padding: props.padding }}>
      {images.map(image => (
        <Grid item lg={6} key={image.title} style={{ width: "100%" }}>
          <Link to={image.link} style={{ textDecoration: "none" }}>
            <ButtonBase
              disableRipple
              disableTouchRipple
              className={classes.image}
              focusVisibleClassName={classes.focusVisible}
              style={{
                width: image.width,
                margin: props.margin,
                boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
              }}
            >
              <SubBody
                url={image.url}
                title={image.title}
                cls={{
                  imageSrc: classes.imageSrc,
                  imageBackdrop: classes.imageBackdrop,
                  imageTitle: classes.imageTitle,
                  imageMarked: classes.imageMarked,
                  imageButton: classes.imageButton
                }} />
            </ButtonBase>
          </Link>
          <Paper style={paper}>
            <h2 style={textColor}>{image.title}</h2>
            <p style={textColor}>
              {image.address}
            </p>
          </Paper>
        </Grid>
      ))}
    </Grid>
    // </Grow>
  );
}

Body.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Body);