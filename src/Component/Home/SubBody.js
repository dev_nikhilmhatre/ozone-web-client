import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';


function Background(props) {
    return (
        <Fragment>
            <span
                className={props.cls.imageSrc}
                style={{
                    backgroundImage: `url(${props.url})`,
                }}
            />
            <span className={props.cls.imageBackdrop} />
        </Fragment>
    )
};

function Button(props) {
    return (
        <span >
            <Typography
                component="span"
                variant="display1"
                color="inherit"
                className={props.cls.imageTitle}>
                {props.title}
                <span className={props.cls.imageMarked} />
            </Typography>
        </span>
    )
}

function SubBody(props) {
    return (
        <Fragment>
            <Background
                url={props.url}
                cls={{
                    imageSrc: props.cls.imageSrc,
                    imageBackdrop: props.cls.imageBackdrop,
                    imageTitle: props.cls.imageTitle,
                    imageMarked: props.cls.imageMarked,
                    imageButton: props.cls.imageButton
                }} />
            <Button
                title={props.title}
                cls={{
                    imageSrc: props.cls.imageSrc,
                    imageBackdrop: props.cls.imageBackdrop,
                    imageTitle: props.cls.imageTitle,
                    imageMarked: props.cls.imageMarked,
                    imageButton: props.cls.imageButton
                }} />
        </Fragment>
    )
}

export default SubBody;