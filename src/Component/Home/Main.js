import React, { Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Body from './Body';
import Logo from '../Logo'


// Header
function Header() {
    return (
        <AppBar position="static" color="inherit">
            <Toolbar>
                <Logo />
                <Typography variant='subheading'
                    color="textSecondary"
                    style={{ width: '100%', textAlign: 'right' }}>
                    <b>
                        Manoj Mhatre
                            <br />
                        9870111183</b>
                </Typography>
            </Toolbar>
        </AppBar>
    )
}

// Footer
// function Footer() {
//     return (
//         <Grid container>
//             <Grid item xs={6} style={{ textAlign: "center" }}>
//                 <Paper style={paper}>
//                     <h2 style={textColor}>Ozone Health Freac</h2>
//                     <p style={textColor}>
//                         Shop No. 6, New Kadambari Society, Naka Signal, Charai,
//                         Thane West, Panch Pakhdi, Thane West, Thane, Maharashtra 400602.
//                         </p>
//                 </Paper>
//             </Grid>

//             <Grid item xs={6} style={{ textAlign: "center" }}>
//                 <Paper style={paper}>
//                     <h2 style={textColor}>Ozone Fitness Center</h2>
//                     <p style={textColor}>
//                         Achalkuti Society, Mahatma Gandhi Rd, Hindu Colony, Shivaji Nagar,
//                         Thane West, Maharashtra 400602.
//                         </p>
//                 </Paper>
//             </Grid>
//         </Grid>
//     );
// };


// Main Page
class Main extends React.Component {
    state = { width: 0, height: 0 }

    componentDidMount() {
        this.updateWindowDimensions()
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        const margin = this.state.width > 600 ? "2%" : "2% 0%"
        const padding = this.state.width > 600 ? "0px" : "10px"
        return (
            <Fragment>
                <Header />
                <Body
                    margin={margin}
                    padding={padding} />
                {/* <Footer /> */}
            </Fragment>
        )
    }
};

export default Main;