import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

// Gym Page
class Gym extends Component {
    render() {
        return (
            <div style={{ textAlign: "center", margin:"10% 1%" }}>
                <Typography variant="display1" gutterBottom>
                    This page is under construction.
                </Typography>
                <Typography variant="subheading" gutterBottom>
                    Please contact &nbsp;
                    <span>
                        <b>
                            Manoj Mhatre: 9870111183
                        </b>
                    </span>
                    &nbsp; for more details about  <strong>Ozone Fitness Center</strong>.
                </Typography>
                <br />
                <br />
                <Link to="/" style={{ textDecoration: "none" }}>
                    <Button variant="contained">
                        Home
                    </Button>
                </Link>
            </div>
        )
    }
};

export default Gym;