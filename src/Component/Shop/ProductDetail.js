import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import { url } from "./../App";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import Grow from '@material-ui/core/Grow';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  toolbar: theme.mixins.toolbar,
  items: {
    fontSize: '1.22rem',
    // fontWeight: "bold",
    color: "rgba(0, 0, 0, 0.54)",
    paddingLeft: "0px",
    paddingTop: "0px",
  }
});

function Header(props) {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="title" color="inherit" className={props.flex}>
          Ozone Health Freac
            </Typography>
        <Button color="inherit">
          <Link to="/shop/" style={{ textDecoration: "none", color: "white" }}>
            Shop
          </Link>
        </Button>
      </Toolbar>
    </AppBar>
  );
};

function Main(props) {
  const data = props.data
  return (
    <main>
      <Paper style={{ margin: "1%", padding: '1%' }}>
        <div></div>
        <Grid container spacing={0}>
          <ProductImage images={data.images} />
          <Info data={data} items={props.items} links={props.links} />
        </Grid>
      </Paper>
    </main>
  );
};

function Stock(props) {
  return (
    <Fragment>
      <Typography variant="title" color="inherit">
        In stock
      </Typography>
      <Typography variant="headline" color="textSecondary">
        {props.flavour} - {props.qty}
      </Typography>
    </Fragment>
  )
}

function Info(props) {
  const data = props.data

  // console.log(props, 'hi')
  return (
    <Grid item lg={8} style={{ width: "100%", padding: 8 }}>
      <Grid container spacing={0}>
        <SubInfo
          brand={data.product_info.product.brand.name}
          name={data.product_info.product.name}
          type={data.product_info.product.is_non_veg}
          weight={data.product_info.weight_lbs}
          mrp={data.product_info.mrp}
          discount={data.product_info.discount}
          qty={data.quantity}
          flavour={data.flavours.name} />
        <KeyFeaturesList
          data={data.product_info.product.key_features.split('|')}
          items={props.items} />
        <OtherDetails
          description={data.product_info.product.description}
          links={props.links} />
      </Grid>
    </Grid>
  )
}

function SubInfo(props) {
  // console.log(props)
  return (
    <Grid item lg={6}>
      <Title
        brand={props.brand}
        name={props.name}
        type={props.type}
        weight={props.weight} />
      <br />
      <Price mrp={props.mrp} discount={props.discount} />
      <br />
      <Stock qty={props.qty} flavour={props.flavour} />
      <br />
    </Grid>
  )
}

function OtherDetails(props) {
  return (
    <Grid container spacing={0}>
      <Grid item lg={12}>
        <Description description={props.description} />
      </Grid>
      <Grid item lg={12}>
        <Links links={props.links} />
      </Grid>
    </Grid>
  )
}

class ProductImage extends Component {

  render() {
    const props = this.props;

    const imgGalleryData = []
    props.images.forEach(image => {
      imgGalleryData.push({
        original: url + image,
        thumbnail: url + image
      })
    });

    return (
      <Grid item lg={4} style={{ width: "100%", padding: "1%", textAlign: "center" }}>
        <Grid container spacing={0}>

          <ImageGallery
            items={imgGalleryData}
            showThumbnails={false}
            showFullscreenButton={false}
            showPlayButton={false} />

        </Grid>
      </Grid>
    );
  }
};

function Type(props) {
  return (
    <Fragment>
      {
        props.type ?
          <span style={{ color: "#DB0101", fontSize: "15px" }}>
            (Non-Veg)
        </span>
          :
          <span style={{ color: "#018C01", fontSize: "15px" }}>
            (Veg)
        </span>
      }
    </Fragment>
  );
};

function Title(props) {
  // console.log(props)
  return (
    <Fragment>
      <Typography variant="title" color="inherit">{props.brand}</Typography>
      <Typography variant="headline" color="textSecondary">
        {props.name + " " + props.weight + ' lbs'}
      </Typography>
      <Type type={props.type} />
      <br />
    </Fragment>
  );
};

function Price(props) {
  // console.log(props)
  return (
    <Fragment>
      <Typography variant="title" color="inherit">
        Price
      </Typography>
      <Typography variant="headline" color="textSecondary">
        {"₹ " + (props.mrp - (props.mrp * props.discount / 100))}
      </Typography>
      <div style={{ fontSize: "16px" }}>
        <span style={{ textDecoration: "line-through", color: "#878787" }}>
          {"₹ " + props.mrp}
        </span>
        <span style={{ color: "green" }}>
          {"  " + props.discount + "% off"}
        </span>
      </div>
    </Fragment>
  )
}

function KeyFeaturesList(props) {
  console.log(props)
  return (
    <Grid item lg={6}>
      <Typography variant="title" color="inherit">
        Key Features
      </Typography>
      <List dense={true}>
        {props.data.map(item => (
          <ListItem
            key={item}
            className={props.items}>
            - {item}
          </ListItem>
        ))}
      </List>
      <br />
    </Grid>
  );
};

// function VariantList(props) {
//   return (
//     <Fragment>
//       <Typography variant="title" color="inherit">
//         Variants
//       </Typography>
//       <List dense={true}>
//         {props.data.map(item => (
//           <ListItem key={item.id} className={props.items}>
//             {item.weight_lbs + " lbs"} / {item.weight_kg + " kgs"}
//           </ListItem>
//         ))}
//       </List>
//     </Fragment>
//   );
// };


// function FlavourList(props) {
//   return (
//     <Fragment>
//       <Typography variant="title" color="inherit">
//         Flavours
//       </Typography>
//       <List dense={true}>
//         {props.data.map(item => (
//           <ListItem key={item.id} className={props.items}>
//             {item.name}
//           </ListItem>
//         ))}
//       </List>
//     </Fragment>
//   );
// };

function Description(props) {
  return (
    <Fragment>
      <Typography variant="title" color="inherit">
        Description
      </Typography>
      <Typography variant="subheading" color="textSecondary"
        style={{
          paddingRight: "5%",
          paddingBottom: "5%"
        }}>
        {props.description}
      </Typography>
    </Fragment>
  )
}

function Links(props) {
  // console.log(props)
  if (props.links.length > 0) {
    return (
      <Fragment>
        <Typography variant="title" color="inherit">
          Related products
        </Typography>
        {
          props.links.map((item, id) => (
            <Fragment key={id}>
              <Link
                key={id}
                to={"/product/" + item.id + '/' + item.product_info.product.id}
                target="_blank"
                style={{ textDecoration: "none" }}>
                <Button size="medium"
                  variant="outlined"
                  color="primary"
                  style={{
                    textTransform: "capitalize",
                    margin: "1%"
                  }}>
                  {
                    item.product_info.product.name
                    + " "
                    + item.product_info.weight_lbs + " lbs "
                    + item.flavours.name
                    + " @ Discount " + item.product_info.discount + "%"
                  }
                </Button>
              </Link>
            </Fragment>
          ))
        }
      </Fragment>
    )
  } else {
    return null
  }

}

class ProductDetail extends Component {
  state = {
    data: {},
    links: []
  }

  getProductDetail = (id, pk) => {
    fetch(url + '/api-product/retrive-c/' + id + '/' + pk)
      .then(res => res.json())
      .then(data => {
        this.setState({ data: { ...data[0] }, links: [...data[1]] })
      })
      .catch(err => console.log(err))
  }

  componentDidMount() {
    this.getProductDetail(this.props.match.params.id, this.props.match.params.pk)
  }

  render() {

    const { classes } = this.props;
    const data = this.state.data;
    const links = this.state.links;

    if (Object.keys(data).length > 0) {
      return (
        <div className={classes.root}>
          <Header flex={classes.flex} />
          <Main data={data} links={links} toolbar={classes.toolbar} items={classes.items} />
        </div >
      );
    }
    else {
      return <div></div>
    }
  }
};

ProductDetail.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductDetail);