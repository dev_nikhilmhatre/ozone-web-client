import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'
import Drawer from '@material-ui/core/Drawer'
import Fab from '@material-ui/core/Fab'
import Phone from '@material-ui/icons/Phone';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from './Sidebar/Sidebar';
import Content from './Content/Content';
import TextField from '@material-ui/core/TextField';
import HocContextApi from "../HocContextApi";
import Button from '@material-ui/core/Button';
import { url } from "../App";
import Tooltip from '@material-ui/core/Tooltip'
import Zoom from '@material-ui/core/Zoom';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: 'auto',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%'
    },
    appBar: {
        position: 'absolute',
        marginLeft: drawerWidth,
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    navIconHide: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        [theme.breakpoints.up('md')]: {
            position: 'relative',
        },
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        height: '90vh',
        overflow: "auto",
        position: "relative"
    },
    searchBarAlightment: {
        minHeight: "20px"
    },
    fab: {
        margin: theme.spacing.unit,
        position: 'fixed',
        bottom: 20,
        right: 20,
        zIndex: 5
    },
    toolTip: {
        fontSize: 16,
        // maxWidth: 500
    }
});

class Shop extends Component {
    state = {
        mobileOpen: false
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    getAllProducts = (page) => {
        fetch(url + '/api-product/list-c/?page=' + page)
            .then(res => res.json())
            .then(data => {
                if (page === 0) {
                    this.props.context.addProducts(data[0])
                } else {
                    this.props.context.updateProducts(data[0])
                }
                this.props.context.updateCount(data[1])
            })
            .catch(err => console.log(err))
    }

    loadMore = (pageNo) => {
        this.getAllProducts(pageNo)
        this.props.context.updatePage(pageNo)
    }

    componentDidMount() {
        this.props.context.updatePage(0)
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>

                {/* Header start */}
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.navIconHide}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" noWrap>
                            Ozone Health Freac
                        </Typography>
                    </Toolbar>
                </AppBar>
                {/* Header end */}

                {/* Mobile view sidebar start */}
                <Hidden mdUp>
                    <Drawer
                        variant="temporary"
                        anchor='left'
                        open={this.state.mobileOpen}
                        onClose={this.handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}>
                        <Sidebar />
                    </Drawer>
                </Hidden>
                {/* Mobile view sidebar end */}

                {/* Desktop view start */}
                <Hidden smDown>
                    <Drawer
                        variant="permanent"
                        open
                        classes={{
                            paper: classes.drawerPaper,
                        }}>
                        <Sidebar />
                    </Drawer>
                </Hidden>
                {/* Desktop view end */}

                {/* Main view sidebar start */}
                <main className={classes.content}>

                    {/* Floating button start */}
                    {
                        window.innerWidth >= 768
                            ? <Tooltip
                                classes={{ tooltip: classes.toolTip }}
                                placement="left"
                                leaveDelay={1000}
                                TransitionComponent={Zoom}
                                title="Manoj Mhatre: 9870-1111-83">
                                <Fab color="primary" className={classes.fab}>
                                    <Phone />
                                </Fab>
                            </Tooltip>
                            : <Fab color="primary" className={classes.fab}>
                                <a href="tel:9870111183" style={{color:'white'}}>
                                <Phone />
                                </a>
                                
                            </Fab>
                    }
                    {/* Floating button end */}

                    <div className={classes.toolbar} />
                    <TextField fullWidth label="Search" onChange={this.props.context.updateSearch} />
                    <div className={classes.searchBarAlightment} />
                    <Content getAllProducts={this.getAllProducts} />
                    <div style={{ textAlign: "center", margin: "15px auto 5px auto" }}>
                        <Button
                            disabled={this.props.context.products.length === this.props.context.count ? true : false}
                            variant="contained"
                            color="primary"
                            onClick={() => this.loadMore(this.props.context.page + 1)}>
                            Load More
                    </Button>
                    </div>
                </main>
                {/* Main view end */}
            </div>
        )
    }
};

Shop.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(HocContextApi(Shop));