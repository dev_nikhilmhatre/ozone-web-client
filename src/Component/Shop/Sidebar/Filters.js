import React, { Fragment } from 'react';
import List from '@material-ui/core/List';
import TextField from '@material-ui/core/TextField';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from "@material-ui/core/ListItemText";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from "@material-ui/core/Checkbox";
import RadioButtonChecked from "@material-ui/icons/RadioButtonChecked";
import RadioButtonUnchecked from "@material-ui/icons/RadioButtonUnchecked";

function PriceFilter(props) {
    return (
        <ListItem style={{ paddingRight: "11px" }}>
            <TextField label="Price" onChange={props.func} />
        </ListItem>
    )
};

function SortingAndRegionFilter(props) {
    const data = props.sorts
    return (
        <Fragment>
            {data.map(items => (
                <Fragment key={items.header}>
                    <ListItem button onClick={items.func}>
                        <ListItemText primary={items.header} />
                        {items.state ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={items.state} timeout="auto">
                        <List component="div" disablePadding>
                            {items.data.map(item => (
                                <ListItem button key={item.id} className={items.cls}>
                                    <ListItemIcon onClick={(e) => items.filterFunc(item.id)}>
                                        {
                                            items.selectedOption === item.id
                                                ? <RadioButtonChecked />
                                                : <RadioButtonUnchecked />
                                        }
                                    </ListItemIcon>
                                    <ListItemText inset primary={item.name} />
                                </ListItem>
                            ))}
                        </List>
                    </Collapse>
                </Fragment>
            ))}
        </Fragment>
    )
}


function OtherFilter(props) {
    return (
        <Fragment>
            <ListItem button onClick={props.func}>
                <ListItemText primary={props.header} />
                {props.state ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <CollapseField
                state={props.state}
                cls={props.cls}
                data={props.data}
                func={props.filterFunc} />
        </Fragment>
    )
};


function CollapseField(props) {
    return (
        <Collapse in={props.state} timeout="auto">
            <List component="div" disablePadding>
                {props.data.map(item => (
                    <ListItem key={item.id} button className={props.cls}>
                        <Checkbox disableRipple onClick={(e) => props.func(item.id, e.target.checked)} />
                        <ListItemText primary={item.name} />
                    </ListItem>
                ))}
            </List>
        </Collapse>
    )
};


function Filter(props) {
    return (
        <div style={{ overflow: 'auto', height: '85vh' }}>
            <PriceFilter func={props.updatePrice} />
            {
                props.data.map(item => (
                    <OtherFilter
                        key={item.header}
                        header={item.header}
                        cls={item.cls}
                        state={item.state}
                        func={item.func}
                        data={item.data}
                        filterFunc={item.filterFunc} />
                ))
            }
            <SortingAndRegionFilter sorts={props.sorts} />
        </div>
    )
};

export default Filter;