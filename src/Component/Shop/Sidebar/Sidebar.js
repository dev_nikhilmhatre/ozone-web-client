import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Filters from './Filters';
import HocContextApi from "../../HocContextApi";
import { url } from "../../App";
import Logo from '../../Logo'

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        // height: '100vh',
        // overflow:'hidden'
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    header: {
        fontSize: "2.3rem",
        lineHeight: "2px"
    },
    helpIcon: {
        paddingLeft: "25px"
    }
})

class Sidebar extends Component {
    state = {
        brandOpen: false,
        categoryOpen: false,
        flavourOpen: false,
        sortingOpen: false,
        otherOpen: false
    };

    handleClickBrand = () => {
        this.setState(state => ({ brandOpen: !state.brandOpen }));
    };

    handleClickCategory = () => {
        this.setState(state => ({ categoryOpen: !state.categoryOpen }));
    };

    handleClickFlavour = () => {
        this.setState(state => ({ flavourOpen: !state.flavourOpen }));
    };

    handleClickSort = () => {
        // console.log('sort')
        this.setState(state => ({ sortingOpen: !state.sortingOpen }));
    };

    handleClickOther = () => {
        // console.log('other')
        this.setState(state => ({ otherOpen: !state.otherOpen }));
    };



    getAllBrands = () => {
        fetch(url + '/api-brand/list-c/')
            .then(res => res.json())
            .then(data => this.props.context.updateBrands(data))
            .catch(err => console.log(err))
    }

    getAllCategories = () => {
        fetch(url + '/api-category/list-c/')
            .then(res => res.json())
            .then(data => this.props.context.updateCategories(data))
            .catch(err => console.log(err))
    }

    getAllFlavours = () => {
        fetch(url + '/api-flavour/list-c/')
            .then(res => res.json())
            .then(data => this.props.context.updateflavours(data))
            .catch(err => console.log(err))
    }

    componentDidMount() {
        this.getAllBrands();
        this.getAllCategories();
        this.getAllFlavours()
    }

    render() {
        // console.log(this.props.context.flavours)

        const { classes } = this.props;
        const data = [
            {
                header: 'Brand',
                cls: classes.nested,
                state: this.state.brandOpen,
                func: this.handleClickBrand,
                data: this.props.context.brands,
                filterFunc: this.props.context.addFilterBrands
            },
            {
                header: 'Category',
                cls: classes.nested,
                state: this.state.categoryOpen,
                func: this.handleClickCategory,
                data: this.props.context.categories,
                filterFunc: this.props.context.addFilterCategories
            },
            {
                header: 'Flavour',
                cls: classes.nested,
                state: this.state.flavourOpen,
                func: this.handleClickFlavour,
                data: this.props.context.flavours,
                filterFunc: this.props.context.addFilterflavours
            }
        ]

        const sortingData = [
            {
                header: 'Sorting',
                cls: classes.nested,
                state: this.state.sortingOpen,
                func: this.handleClickSort,
                data: [
                    { 'id': '-views', name: 'Popularity' },
                    { 'id': 'product_info__mrp', name: 'Price - Low to High' },
                    { 'id': '-product_info__mrp', name: 'Price - High to Low' }
                ],
                filterFunc: this.props.context.updateSorting,
                selectedOption: this.props.context.sorting

            },
            {
                header: 'Other',
                cls: classes.nested,
                state: this.state.otherOpen,
                func: this.handleClickOther,
                data: [
                    { 'id': 'all', name: 'Show All' },
                    { 'id': 'India', name: 'Indian Products' },
                    { 'id': 'Other', name: 'Non-Indian Products' }
                ],
                filterFunc: this.props.context.updateOther,
                selectedOption: this.props.context.other

            }
        ]

        return (
            <div className={classes.root}>
                <List
                    component="nav"
                    subheader={
                        <div style={{ padding:'8px 3px'}}>
                            <Link to="/"
                                style={{ textDecoration: "none" }}>
                                <Logo />
                            </Link>
                        </div>
                    }>
                    <Divider />
                    <Filters
                        data={data}
                        sorts={sortingData}
                        updatePrice={this.props.context.updatePrice} />
                </List>
            </div>
        )
    }
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HocContextApi(Sidebar));