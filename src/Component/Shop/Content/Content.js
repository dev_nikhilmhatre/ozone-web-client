import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import Typography from '@material-ui/core/Typography';
import HocContextApi from "../../HocContextApi";
import { url } from "../../App";
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

const styles = () => ({
    card: {
        maxWidth: 400,
        margin: "auto"
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    }
});

class Content extends Component {
    componentDidMount() {
        this.props.getAllProducts(0);
    }

    render() {
        const { classes } = this.props;
        return (
            <Grid container spacing={24}>
                {
                    this.props.context.products.map(item => (
                        <Products
                            key={item.id}
                            data={item}
                            cls={{
                                card: classes.card,
                                media: classes.media
                            }} />
                    ))
                }
            </Grid>
        )
    }
};


function Products(props) {
    return (
        <Grid item lg={4} style={{ width: "100%" }}>
            <Card className={props.cls.card}>
                <CardHeader
                    title={props.data.product_info.product.name}
                    subheader={props.data.flavours.name + " - " + props.data.product_info.weight_lbs + " lbs"}
                />
                <CardMedia
                    className={props.cls.media}
                    image={url + props.data.images[0]}
                />
                <CardActions>
                    <Grid container spacing={8}>
                        <Grid item xs={9}>
                            <Typography variant="title">
                                {"₹ " + (props.data.product_info.mrp - (props.data.product_info.mrp * props.data.product_info.discount / 100))}
                            </Typography>
                            <div style={{ fontSize: "14px" }}>
                                <span style={{ textDecoration: "line-through", color: "#878787" }}>
                                    {"₹ " + props.data.product_info.mrp}
                                </span>
                                <span style={{ color: "green" }}>
                                    {"  " + props.data.product_info.discount + "% off"}
                                </span>
                            </div>
                        </Grid>
                        <Grid item xs={3}>
                            <Link to={"/product/" + props.data.id + '/' + props.data.product_info.product.id} target="_blank" style={{ textDecoration: "none" }}>
                                <Button size="small" variant="outlined" color="primary" style={{ textTransform: "capitalize" }}>
                                    More
                                </Button>
                            </Link>
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        </Grid >
    )
};

Content.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(HocContextApi(Content));