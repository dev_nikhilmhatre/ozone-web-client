import React, { Component, createContext } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import Main from './Home/Main';
const Main = React.lazy(() => import('./Home/Main'))
// import Shop from './Shop/Shop';
const Shop = React.lazy(() => import('./Shop/Shop'))
// import Gym from './Gym/Gym';
const Gym = React.lazy(() => import('./Gym/Gym'))
// import ProductDetail from './Shop/ProductDetail';
const ProductDetail = React.lazy(() => import('./Shop/ProductDetail'))
// import Error404 from '../404'
const Error404 = React.lazy(() => import('../404'))

//base url
// export const url = 'http://13.234.16.252:8001' // prod
export const url = 'http://127.0.0.1:8000' // dev


// Context Api
export const Context = createContext()

class Provider extends Component {

  state = {
    // pagination
    page: 0,
    updatePage: (data) => this.setState({ page: data }),
    count: 0,
    updateCount: (data) => this.setState({ count: data }),

    // global state for Brand
    brands: [],
    updateBrands: (data) => this.setState({ brands: [...data] }),
    filterBrands: [],
    addFilterBrands: (data, bool) => {
      let filterOpt = [...this.state.filterBrands]
      if (bool) {
        filterOpt = [...filterOpt, data]
        this.setState({ filterBrands: [...filterOpt] })
      }
      else {
        filterOpt = filterOpt.filter(item => item !== data)
        this.setState({ filterBrands: [...filterOpt] })
      }
      this.filterResult(
        this.state.price,
        filterOpt,
        this.state.filterCategories,
        this.state.search,
        this.state.filterflavours,
        this.state.sorting,
        this.state.other
      )
    },

    // global state for Category
    categories: [],
    updateCategories: (data) => this.setState({ categories: [...data] }),
    filterCategories: [],
    addFilterCategories: (data, bool) => {
      let filterOpt = [...this.state.filterCategories]
      if (bool) {
        filterOpt = [...filterOpt, data]
        this.setState({ filterCategories: [...filterOpt] })
      }
      else {
        filterOpt = filterOpt.filter(item => item !== data)
        this.setState({ filterCategories: [...filterOpt] })
      }
      this.filterResult(
        this.state.price,
        this.state.filterBrands,
        filterOpt,
        this.state.search,
        this.state.filterflavours,
        this.state.sorting,
        this.state.other
      )
    },

    // global state for Flavours
    flavours: [],
    updateflavours: (data) => this.setState({ flavours: [...data] }),
    filterflavours: [],
    addFilterflavours: (data, bool) => {
      let filterOpt = [...this.state.filterflavours]
      if (bool) {
        filterOpt = [...filterOpt, data]
        this.setState({ filterflavours: [...filterOpt] })
      }
      else {
        filterOpt = filterOpt.filter(item => item !== data)
        this.setState({ filterflavours: [...filterOpt] })
      }
      this.filterResult(
        this.state.price,
        this.state.filterBrands,
        this.state.filterCategories,
        this.state.search,
        filterOpt,
        this.state.sorting,
        this.state.other
      )
    },

    // global state for Product
    products: [],
    addProducts: (data) => this.setState({ products: [...data] }),
    updateProducts: (data) => this.setState(prevState => ({
      products: [...prevState.products, ...data]
    })),
    filterProducts: [],

    // Price
    price: '',
    updatePrice: (e) => {
      this.setState({ price: e.target.value })
      this.filterResult(
        e.target.value,
        this.state.filterBrands,
        this.state.filterCategories,
        this.state.search,
        this.state.filterflavours,
        this.state.sorting,
        this.state.other
      )
    },

    // Search
    search: '',
    updateSearch: (e) => {
      this.setState({ search: e.target.value })
      this.filterResult(
        this.state.price,
        this.state.filterBrands,
        this.state.filterCategories,
        e.target.value,
        this.state.filterflavours,
        this.state.sorting,
        this.state.other
      )
    },

    // sorting
    sorting: '-views',
    updateSorting: (data) => {
      this.setState({ sorting: data })
      this.filterResult(
        this.state.price,
        this.state.filterBrands,
        this.state.filterCategories,
        this.state.search,
        this.state.filterflavours,
        data,
        this.state.other
      )
    },

    // other
    other: 'all',
    updateOther: (data) => {
      this.setState({ other: data })
      this.filterResult(
        this.state.price,
        this.state.filterBrands,
        this.state.filterCategories,
        this.state.search,
        this.state.filterflavours,
        this.state.sorting,
        data
      )
    }
  }

  filterResult = (
    price = '',
    brands = [],
    categories = [],
    search = '',
    flavours = [],
    sort,
    other
  ) => {

    if (price.length === 0) {
      price = ''
    }
    if (brands.length === 0) {
      brands = []
    }
    if (categories.length === 0) {
      categories = []
    }
    if (search.length === 0) {
      search = ''
    }
    if (flavours.length === 0) {
      flavours = []
    }

    let fetchCall;
    if (price === ''
      && brands.length === 0
      && categories.length === 0
      && search === ''
      && flavours.length === 0
      && sort === '-views'
      && other === 'all'
    ) {
      fetchCall = fetch(url + '/api-product/list-c/?page=0')
      this.setState({ page: 0 })
    }
    else {
      fetchCall = fetch(url + '/api-product/product-search-c/',
        {
          method: "POST",
          body: JSON.stringify({
            mrp: price,
            brands: brands,
            category: categories,
            search: search,
            flavours: flavours,
            sorting: sort,
            other: other
          }),
          headers: {
            "Content-Type": "application/json",
            'Accept': 'application/json'
          }
        })
    }
    fetchCall.then((Response) => Response.json())
      .then(data => {
        this.setState({ products: [...data[0]] })
        this.setState({ count: data[1] })
      })
      .catch(err => {
        console.log(err)
      })
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    )
  }
}

// Main App
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider>
          <React.Suspense fallback={<Demo />}>
            <Switch>
              <Route exact path='/' render={() => <Main />} />
              <Route exact path='/shop/' render={() => <Shop />} />
              <Route exact path='/gym/' render={() => <Gym />} />
              <Route exact path='/product/:id/:pk' render={(routeProps) => <ProductDetail {...routeProps} />} />
              <Route render={() => <Error404 />} />
            </Switch>
          </React.Suspense>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;


const Demo = () => (
  <div></div>
)