import React from 'react';


// Loader
function Loader() {
    return (
        <div id="loader">
            <div id="shadow"></div>
            <div id="box"></div>
        </div>
    )
};

export default Loader;